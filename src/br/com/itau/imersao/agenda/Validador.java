package br.com.itau.imersao.agenda;

import java.util.Scanner;

public class Validador {

	public static long validaNumero(String texto, Scanner scanner) {
		try {
			long numero = 0;
			
			numero = Long.parseLong(texto);

			return numero;

		} catch (Exception e) {
			Impressora.enviaMensagem("Formato do numero esta incorreto, poderia digitar novamente, por gentileza?");
			texto = scanner.nextLine();
			return validaNumero(texto, scanner);
		}
	}
	
	public static boolean desejaNovoContato(Scanner scanner) {
		String texto = "";
		Impressora.enviaMensagem("Deseja adicionar um novo contato(y/n)?");
		texto = scanner.nextLine();
		
		if (texto.equals("y")) {
			return true;
		}
		else if (texto.equals("n")){
			return false;
		}
		else {
			Impressora.enviaMensagem("Ação não especificada");
			return desejaNovoContato(scanner);
		}
	}

}
