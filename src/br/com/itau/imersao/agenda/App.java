package br.com.itau.imersao.agenda;

import java.util.Scanner;

public class App {

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		String nome = "";
		boolean novoContato = true;
		Agenda agenda = new Agenda();
		
		Impressora.enviaMensagem("Olá, sou sua agenda telefonica");
		Impressora.enviaMensagem("Vamos, começar, me diga qual seu nome");
		
		nome = scanner.nextLine();
				
		while (novoContato) {
			Contato contato = new Contato();
			Impressora.enviaMensagem("Qual o nome do novo contato?");
			contato.nome = scanner.nextLine();
			
			Impressora.enviaMensagem("Qual é o número deste novo contato?");
			contato.numero = Validador.validaNumero(scanner.nextLine(), scanner);
			
			agenda.contatos.add(contato);
			
			Impressora.enviaMensagem("Contato adicionado");
			
			novoContato = Validador.desejaNovoContato(scanner);
		}
		
		Impressora.imprimiAgenda(agenda);

	}

}
