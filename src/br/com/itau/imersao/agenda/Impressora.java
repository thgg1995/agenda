package br.com.itau.imersao.agenda;

import java.util.ArrayList;

public class Impressora {

	public static void enviaMensagem(String texto) {
		System.out.println(texto);
	}

	public static void imprimiListaAgenda(ArrayList<Agenda> agenda) {
		for (int i = 0; i < agenda.size(); i++) {
			for (int j = 0; j < agenda.get(i).contatos.size(); j++) {
				System.out.println("Contato:");
				System.out.println("Nome: " + agenda.get(i).contatos.get(j).nome);
				System.out.println("Telefone: " + +agenda.get(i).contatos.get(j).numero);
			}
		}
	}

	public static void imprimiAgenda(Agenda agenda) {
		for (int i = 0; i < agenda.contatos.size(); i++) {
			System.out.println("Contato " + i + ":");
			System.out.println("Nome: " + agenda.contatos.get(i).nome);
			System.out.println("Telefone: " + +agenda.contatos.get(i).numero);
		}
	}

}
